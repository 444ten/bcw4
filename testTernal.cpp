#include <iostream>

int main() {
    std::string msg = true ? "Yes" : "No";

    std::cout << msg << std::endl;

    return 0;
}